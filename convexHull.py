import numpy as np
import matplotlib.pyplot as plt

def plot_rand(npoints):
    points_rand = np.random.rand(npoints, 2)   # 100 random points in 2-D (n puntos, dimensiones)
    #x_rand = points_rand[:,0]
    #y_rand = points_rand[:,1]
    return points_rand
    #plt.plot(x_rand, y_rand, 'o')
    #plt.show()

def determinant(p0, p1, p2):
    sum1 = p1[0]*p2[1]+p0[0]*p1[1]+p2[0]*p0[1]
    sum2 = p1[0]*p0[1]+p2[0]*p1[1]+p0[0]*p2[1]
    return sum1-sum2

def onSegment(p0, p1, p2):
    #if min(p0[0], p1[0])<=p2[0] && p2[0]<=max(p0[0], p1[0]) && min(p0[1], p1[1])<=p2[1] && p2[1]<=max(p0[1], p1[1]):
    #    return True
    #else:
    #    return False
    if determinant(p0, p1, p2)<0:
        return 0 #Sentido antihorario
    else:
        return 1 #Sentido horario si >0, si 0, son colineales

#def merge(left, right):

def convexHull(points):
    points.sort()
    for point in points:
        print(point)


    plt.plot(points, 'x')
    plt.show()

    left = [points[0], points[1]]
    for i in range(2, int(len(points)/2), 1):
        left.append(points[i])
        while len(left) > 2 and not onSegment(left[0], left[1], left[2]):
            left.pop()

    right = [points[int(len(points)/2)+1], points[int(len(points)/2)+2]]
    for i in range (int(len(points)/2)+3, int(len(points)), 1):
        while len(right) > 2 and not onSegment(right[0], right[1], right[2]):
            right.pop()

    #Concatenacion
    del left[0]
    del left[-1]

    return tuple(right + left)

points = plot_rand(10)

for point in points:
    print(point)
print()
plt.plot(points, 'o')
plt.show()
convexHull(points)
